const
    dto = require('../common/requireDir')('../dto'),
    config = require('../config/config'),
    cacher = require('../common/sequelizeRedisCashPostgresConnection').cacher,
    Post = require('../common/sequelizeModels').Post,
    Vote = require('../common/sequelizeModels').Vote,
    moment = require('moment');

module.exports = {

    get: function (id, callback, error) {
        Post.find({
            where: {id: id}
        }).then(
            (posts) => callback(posts));
    },

    getTop: function (data, callback, error) {
        if (data) {
            var offset = data.offset;
            var limit = data.limit < config.getTopPostLimit ?
                data.limit : config.getTopPostLimit;
        } else {
            var offset = config.getTopPostOffset;
            var limit = config.getTopPostLimit;
        }
        cacher.model('post').findAll({
            offset: offset,
            limit: limit,
            order: [['rate', 'DESC']]
        }).then(
            (posts) => callback(posts),
            (err) => error(err)
        )
    },

    create: function (post, currentUser, callback, error) {
        var date = Math.floor(moment.now() / 1000);
        Post.create({
            userId: currentUser.id,
            date: date,
            title: post.title,
            text: post.text,
            upvotes: 0,
            downvotes: 0,
            rate: date
        }).then(
            (post) => callback(post),
            (err) => error(err)
        );
    },

    update: function (data, currentUser, callback, error) {
        Post.update({
            text: data.text
        }, {
            where: {
                id: data.id,
                userId: currentUser.id
            }
        }).then(
            (count) => {
                (count > 0) ?
                callback(count) :
                error('Post was not found or written by other user')
            },
                    (err) => error(err)
        )

    },

    upvote: function (id, currentUser, callback, error) {
        tovote('upvotes', config.genPosts.upvoteEquilsSeconds, id, currentUser, callback, error)
    },

    downvote: function (id, currentUser, callback, error) {
        tovote('downvotes', -config.genPosts.downvoteEquilsSeconds, id, currentUser, callback, error)
    },

    remove: function (id, currentUser, callback, error) {
        Post.destroy({
            where: {
                id: id,
                userId: currentUser.id
            }
        }).then(
            () => {
                Vote.destroy({
                    where: {
                        postId: id
                    }
                }).then(
                    (quantity) => callback('Post was deleted successfully. ' + quantity + ' votes were removed'),
                    (err) => error(err)
                )
            },
            (err) => error(err)
        );
    },

};

function tovote(command, seconds, id, currentUser, callback, error) {

    Vote.count({
        where: {
            userId: currentUser.id,
            postId: id
        }
    }).then(
        (count) => {
            if (count == 0) {
                Vote.create({
                    userId: currentUser.id,
                    postId: id
                }).then(
                    (vote) => {
                        Post.increment(command, {where: {id: vote.postId}}).then(
                            () => {
                                Post.increment('rate', {by: seconds, where: {id: vote.postId}}).then(
                                    (post) => callback(post),
                                    (err) => error(err)
                                )
                            },
                            (err) => error(err)
                        )
                    },
                    (err) => error(err)
                );
            } else {
                error('User already voted this post')
            }
        },
        (err) => error(err)
    )
}