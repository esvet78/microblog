const
    dto = require('../common/requireDir')('../dto'),
    cacher = require('../common/sequelizeRedisCashPostgresConnection').cacher,
    User = require('../common/sequelizeModels').User;

module.exports = {

    register: function (data, callback, error) {
        User.find({
            where: {
                name: data.name
            }
        }).then(
            (user) => {
                if (user) {
                    return error('User already exists')
                } else {
                    User.create({
                        name: data.name,
                        password: data.password
                    }).then(
                        (user) => {
                            (user) ?
                                callback(new dto.CurrentUser(user.id, user.name)) :
                                error('User was not registered')
                        },
                        (err) => error(err)
                    )
                }
            },
            (err) => error(err)
        )
    },

    login: function (data, callback, error) {
        User.find({
            where: {
                name: data.name,
                password: data.password,
            }
        }).then(
            (user) => {
                (user) ?
                    callback(new dto.CurrentUser(user.id, user.name)) :
                    error('User was not logged in')
            },
            (err) => error(err)
        )
    }
}
