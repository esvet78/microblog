const
    Sequelize = require('./sequelizeRedisCashPostgresConnection').Sequelize,
    sequelize = require('./sequelizeRedisCashPostgresConnection').sequelize;

module.exports.User = sequelize.define('user', {
    id: {
        type: Sequelize.INTEGER, autoIncrement: true, primaryKey: true
    },
    name: {
        type: Sequelize.STRING
    },
    password: {
        type: Sequelize.STRING
    }
});

module.exports.Post = sequelize.define('post', {
        id: {
            type: Sequelize.INTEGER, autoIncrement: true, primaryKey: true
        },
        userId: {
            type: Sequelize.INTEGER
        },
        date: {
            type: Sequelize.INTEGER
        },
        title: {
            type: Sequelize.STRING
        },
        text: {
            type: Sequelize.TEXT
        },
        upvotes: {
            type: Sequelize.INTEGER
        },
        downvotes: {
            type: Sequelize.INTEGER
        },
        rate: {
            type: Sequelize.INTEGER
        }
    },
    {
        indexes: [{
            name: 'rate_idx_desc',
            unique: false,
            fields: [{attribute: 'rate', order: 'DESC'}]
        }]
    });

module.exports.Vote = sequelize.define('vote', {
    userId: {
        type: Sequelize.INTEGER
    },
    postId: {
        type: Sequelize.INTEGER
    }
});
