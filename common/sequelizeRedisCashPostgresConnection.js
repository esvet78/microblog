const
    cacher = require('sequelize-redis-cache'),
    redis = require('redis'),
    Sequelize = require('sequelize'),
    config = require('../config/config');
module.exports.Sequelize = Sequelize;

var rc = redis.createClient(config.redisPort, config.redisHost);
sequelize = module.exports.sequelize =
    new Sequelize('postgres://' + config.pgUser + ':' + config.pgPassword + '@' +
        config.pgHost + ':' + config.pgPort + '/' + config.pgDBName,{logging: false});
module.exports.cacher = cacher(sequelize, rc)
    .ttl(5);