const
    elasticsearch = require('elasticsearch'),
    config = require('../config/config');
var host = config.dbip + ':' + config.dbport;
module.exports.client = elasticsearch.Client({
    host: host
});