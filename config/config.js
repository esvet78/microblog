// module.exports.dbip = '127.0.0.1';
// module.exports.dbport = 9200;

module.exports.host = process.env.HOST ? process.env.HOST : '127.0.0.1';
module.exports.port = process.env.PORT ? process.env.PORT : 8081;
module.exports.redisHost = process.env.REDIS_HOST ? process.env.REDIS_HOST : '127.0.0.1'
module.exports.redisPort = process.env.REDIS_PORT ? process.env.REDIS_PORT : 6379
module.exports.pgHost = process.env.PG_HOST ? process.env.PG_HOST : '127.0.0.1'
module.exports.pgPort = process.env.PG_PORT ? process.env.PG_PORT : 5432

module.exports.pgUser = process.env.PG_USER ? process.env.PG_USER : 'gis';
module.exports.pgPassword = process.env.PG_PASS ? process.env.PG_PASS : 'password';
module.exports.pgDBName = process.env.PG_DBNAME ? process.env.PG_DBNAME : 'microblog';

    module.exports.genPosts = {
    createPostTable: true,
    maxPosts: 2000,
    minTitleSize: 3,
    maxTitleSize: 8,
    minTextSize: 20,
    maxTextSize: 100,
    minDate: 1518000000,
    maxDate: 1520000000,
    maxUpvotes: 5,
    maxDownvotes: 5,
    upvoteEquilsSeconds: 60,
    downvoteEquilsSeconds: 60
}

module.exports.genUsers = {
    createUserTable: true,
    maxUsers: 10000,
    minNameSize: 8,
    maxNameSize: 12,
    minPasswordSize: 8,
    maxPasswordSize: 12
}

module.exports.genVotes = {
    createVoteTable: true,
}

module.exports.getTopPostOffset = 0;
module.exports.getTopPostLimit = 1000;
